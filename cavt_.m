function [tmax] = script_cav_t()

    tmax = inf;
    mesh_nr = 100;

    %Geometry Description
    R1 = [3 4 -0.5 0.5 0.5 -0.5 1 1 0 0];
    R2 = [3 4 -0.050000000000000003 0.050000000000000003 0.050000000000000003 -0.050000000000000003 0.59999999999999998 0.59999999999999998 0 0];
    % T-Shaped
    R3 = [3 4 -0.29999999999999999 0.29999999999999999 0.29999999999999999 -0.29999999999999999 0.69999999999999996 0.69999999999999996 0.59999999999999998 0.59999999999999998];
    % I-Shaped cavity
    % R3 = [3 4 -0.05 0.05 0.05 -0.05 0.69999999999999996 0.69999999999999996 0.59999999999999998 0.59999999999999998];
    
    %Geometry Formula
    ns = (char('R1','R2','R3'))';
    sf = 'R1-(R2+R3)';
    gd = [R1', R2', R3'];
    dl = decsg(gd,sf,ns); %%Decomposed geometry matrix : https://la.mathworks.com/help/pde/ug/decsg.html#bu_fft3-dl
    
    % PDE Model
    model = createpde(1);
    pg = geometryFromEdges(model,dl);

    figure(1);
    pdegplot(model,'EdgeLabels','on');

    geoSize = length(dl);
    solidEdges = [];% solid edges
    et = 1e-15; % truncate error
    for i=1:geoSize
        vert = dl(:,i);
        if or((abs(vert(3) + R2(4)) < et && abs(vert(2) + R1(4)) < et),(abs(vert(2) + R2(4)) < et && abs(vert(3) + R1(4)) < et))
            solidEdges = [solidEdges i];
        elseif abs(vert(2) + R1(4)) < et && abs(vert(3) + R1(4)) < et
            solidEdges = [solidEdges i];
        elseif or(abs(vert(2) - R1(4)) < et && abs(vert(3)+R1(4)) < et,abs(vert(2) +R1(4)) < et && abs(vert(3)-R1(4)) < et)  %seg superior
            solidEdges = [solidEdges i];
		elseif abs(vert(2) - R1(4)) < et && abs(vert(3)-R1(4)) < et %seg lateral positivo
            solidEdges = [solidEdges i];
		elseif or((abs(vert(3)-R1(4)) < et && abs(vert(2)-R2(4)) < et ),(abs(vert(2)-R1(4)) < et && abs(vert(3)-R2(4)) < et)) %seg inferior positivo
            solidEdges = [solidEdges i];
        end
    end
    cavityEdges = [];
    for i=1:geoSize
        if i ~= solidEdges(1) && i ~=  solidEdges(2) && i ~=  solidEdges(3) && i ~= solidEdges(4) &&  i ~= solidEdges(5)
			 cavityEdges = [ cavityEdges i];
		end
    end
    solidEdges 
    cavityEdges

    applyBoundaryCondition(model,'neumann','Edge',solidEdges,'q',0,'g',0);
	applyBoundaryCondition(model,'dirichlet','Edge',cavityEdges,'h',1,'r',0);
	specifyCoefficients(model,'m',0,'d',0,'c',1,'a',0,'f',1);	
	
    generateMesh(model,'Hmax',1/mesh_nr,'Hgrad',1.3);

    figure(2);
    pdemesh(model);

    results = solvepde(model);
	u = results.NodalSolution;

    figure(3);
    pdeplot(model,'XYData',u,'colormap','jet','contour','off');	

	tmax = max(u);
end